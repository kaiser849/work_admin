package kr.admin.work.service;

import kr.admin.work.mapper.UserMapper;
import kr.admin.work.vo.UserInfoVO;
import kr.admin.work.vo.UserSecureVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class SecurityService implements UserDetailsService {

    private UserMapper mapper;

    @Autowired
    public void setUserMapper(UserMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Map<String, Object> param = new HashMap<>();
        param.put("userId", username);
        UserInfoVO user = null;

        try {
             user = mapper.getuserInfo(param);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return new UserSecureVO(user);
    }
}
