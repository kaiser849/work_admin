package kr.admin.work.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

    @RequestMapping("/page/list")
    public ModelAndView pageView(){

        ModelAndView view = new ModelAndView();
        view.setViewName("views/list");
        return view;
    }


    @RequestMapping("/admin/main")
    public ModelAndView adminMain(){

        ModelAndView view = new ModelAndView();
        view.setViewName("views/admin/adminMain");
        return view;
    }
}
