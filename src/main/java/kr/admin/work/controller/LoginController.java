package kr.admin.work.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import kr.admin.work.security.ZRsaSecurity;
import kr.admin.work.vo.UserInfoVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/main")
public class LoginController {

    private Logger logger = LoggerFactory.getLogger(LoginController.class);


    @Autowired
    AuthenticationManager authenticationManager;

    @RequestMapping("/loginView")
    public ModelAndView pageView(HttpServletRequest request,
                                  @RequestParam(value = "errorCode", required = false) String errorCode){

        ModelAndView view = new ModelAndView();
        //이전 페이지 저장
        String referrer = request.getHeader("Referer");
        request.getSession().setAttribute("prevPage", referrer);
        view.setViewName("views/login");
        return view;
    }

    @GetMapping(value = "/err/denied-page")
    public ModelAndView  accessDenied(){
        ModelAndView view = new ModelAndView();
        view.setViewName("error/deniedPage");
        return view;
    }




    @RequestMapping("/getSecureKey")
    @ResponseBody
    public Map<String, Object> getRSAKeyValue(HttpServletRequest req, HttpServletResponse res) throws Exception {

        Map<String, Object> resultMap = new HashMap<>();

        try {

            ZRsaSecurity zSecurity = new ZRsaSecurity();
            PrivateKey privateKey = zSecurity.getPrivateKey();

            HttpSession session = req.getSession();

            if(session.getAttribute("_rsaPrivateKey_") !=null) {
                session.removeAttribute("_rsaPrivateKey_");
            }

            session.setAttribute("_rsaPrivateKey_", privateKey);

            String publicKeyModulus = zSecurity.getRsaPublicKeyModulus();
            String publicKeyExponent = zSecurity.getRsaPublicKeyExponent();

            resultMap.put("publicKeyModulus", publicKeyModulus);
            resultMap.put("publicKeyExponent", publicKeyExponent);

        }catch (Exception e){
            logger.error("error", e.getMessage());
        }

        return resultMap;
    }


    @RequestMapping(value = "/loginajax", method = RequestMethod.POST)
    @ResponseBody
    public ModelMap login(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam(value = "userId") String username,
                          @RequestParam(value = "userKey") String password) {

        ModelMap map = new ModelMap();

        try {

            ZRsaSecurity rsa = new ZRsaSecurity();

            HttpSession session = request.getSession();
            PrivateKey privateKey = (PrivateKey) session.getAttribute("_rsaPrivateKey_");

            String userId = rsa.decryptRSA(privateKey, username);
            String userKey = rsa.decryptRSA(privateKey, password);



            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userId, userKey);

            // 로그인
            Authentication auth = authenticationManager.authenticate(token);
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(auth);


            session.removeAttribute("_rsaPrivateKey_"); // 키의 재사용을 막는다. 다만 로그인되기전까지만 유지...
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);


            map.put("resultCode", 200);
            map.put("resultMsg", "OK");
            map.put("returnUrl", getReturnUrl(request, response));
        } catch (Exception e) {
            map.put("resultCode", 500);
            map.put("resultMsg", e.getMessage());
        }

        return map;
    }


    /**
     * 로그인 하기 전의 요청했던 URL을 알아낸다.
     *
     * @param request
     * @param response
     * @return
     */
    private String getReturnUrl(HttpServletRequest request, HttpServletResponse response) {
        RequestCache requestCache = new HttpSessionRequestCache();
        String referer = request.getSession().getAttribute("prevPage") == null ? "" :request.getSession().getAttribute("prevPage").toString() ;
        SavedRequest savedRequest = requestCache.getRequest(request, response);

        String path = request.getRequestURL().toString();
        String url = request.getRequestURI();
        String defaultPath = path.replace(url, "/");

        if (savedRequest == null || savedRequest.getRedirectUrl().equals(defaultPath)) {
            return "/page/list";
        }
        return savedRequest.getRedirectUrl();
    }
}
