package kr.admin.work;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorkAdminApplication.class, args);
    }

}
