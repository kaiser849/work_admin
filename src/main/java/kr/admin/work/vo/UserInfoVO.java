package kr.admin.work.vo;

import lombok.Data;
import org.springframework.security.core.userdetails.UserDetails;

@Data
public class UserInfoVO{

    private String userId;
    private String userPw;
    private String userRole;

}
