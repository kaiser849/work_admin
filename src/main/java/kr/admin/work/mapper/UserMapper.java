package kr.admin.work.mapper;

import kr.admin.work.vo.UserInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
@Mapper
public interface UserMapper {
    public UserInfoVO getuserInfo(Map<String, Object> param) throws Exception;
}
