package kr.admin.work.config;

import kr.admin.work.handler.LogOutSuccessHandler;
import kr.admin.work.handler.LoginFailureHandler;
import kr.admin.work.handler.LoginSuccessHandler;
import kr.admin.work.handler.WebAccessDeniedHandler;
import kr.admin.work.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@Order(SecurityProperties.DEFAULT_FILTER_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	//로그인 성공 시 호출되는 핸들
	@Autowired
	LogOutSuccessHandler logOutSuccessHandler;

	//userDetailService 를 상속받아 구현한 서비스
	@Autowired
	SecurityService service;

	/**
	 * 권한 체크를 무시할 경로를 지정
	 * 시큐리티를 타지 말아야할 경로들이 있다.
	 * 대부분 resources 관련 경로들이나 로그인페이지 등.
	 * @param web
	 * @throws Exception
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
				.antMatchers("/css/**",   "/js/**",  "/images/**")
				.antMatchers("/fonts/**", "/lib/**", "/webjars/**")
				.antMatchers("/main/**");
	}


	/**
	 * 시큐리티 보안설정
	 * @param http
	 * @throws Exception
	 */
/*	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http.csrf().disable()
				.authorizeRequests()
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll() // 오픈 API에서 가끔 요청하는거라 권한을 푼다. - 로그인안해도 볼수 있다.
				.antMatchers("/admin/**").hasRole("ADMIN") // admin 경로는 admin권한자만 볼수있다.
				.anyRequest().authenticated() // 모든 경로는 권한 부여
				.and()  // 주제 전환용 큰 의미 없음.
				.formLogin()
				.loginPage("/main/loginView")
				.loginProcessingUrl("/main/loginajax")  // 실제 로그인을 처리할 경로 지정 - 여기선 커스텀 경로
				.and()
				.logout().invalidateHttpSession(true) // 로그 아웃 시 세션을 지운다.
				.clearAuthentication(true) // 로그 아웃시 권한 삭제
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessHandler(logOutSuccessHandler).permitAll() // 로그아웃이 되면 실행할 핸들러 등록
				.and().exceptionHandling().accessDeniedHandler(new WebAccessDeniedHandler()); // 권한이 없는 페이지를 보려했을 때 처리할 핸들러 등록 

	}*/

	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http.csrf().disable()
				.authorizeRequests()
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
				.antMatchers("/admin").hasAnyRole("ADMIN")
				.anyRequest().authenticated()
				.and()
				.formLogin()
				.loginPage("/main/loginView")
				.loginProcessingUrl("/login").permitAll()
				.successHandler(successHandler())
				.failureHandler(new LoginFailureHandler())
				.and()
				.logout().invalidateHttpSession(true)
				.clearAuthentication(true)
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessHandler(logOutSuccessHandler).permitAll();
	}

	/**
	 * 시큐리티 암호화사용을 위한 빈 등록
	 * @return
	 */
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}


	/**
	 *  스프링 시큐리티 처리하는 프로바이더
	 *
	 * @return
	 */
	@Bean
	public AuthenticationProvider authProvider()
	{
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setUserDetailsService(service);
		provider.setPasswordEncoder(passwordEncoder());
		return provider;
	}

	/**
	 * AuthenticationManager  를
	 * 사용하기 위해서 오버라이드
	 * @return
	 */
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public AuthenticationSuccessHandler successHandler() {
		return new LoginSuccessHandler();
	}


}


